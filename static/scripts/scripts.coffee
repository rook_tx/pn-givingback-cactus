Main = (()->

  init = ()->

    ## Event listeners ##

    $(()->
      $c(document).trigger('Main.ready')
    )

    $c(document).on 'Main.ready', ()->
      $c(document).trigger('Main.update')

    $c(document).on 'Main.update', ()->
      $('body').removeClass('loading')

    $(document).find('.sec-copy, .sec-pullquote, .sec-image-title').each () ->
      $(this).find('h2, h3').attr('data-bottom-top','opacity:0;transform:translate3d(20px,0,0)').attr('data-center','opacity:1;transform:translate3d(0px,0,0)')
      $(this).find('h5').attr('data-bottom-top','opacity:0;transform:translate3d(0,20px,0)').attr('data-center','opacity:1;transform:translate3d(0,0px,0)')

    $c(document).on 'click', '[data-nohref="true"]', (e)->
      e.preventDefault()

    #   # $mnav = $(this).parents('.mnav')
    #   $mnav = $(this)
    #   height = $mnav.find('.mnav-body').outerHeight()
    #   width = $mnav.find('.mnav-body').outerWidth()
    #
    #   if $mnav.hasClass 'mnav-closed'
    #     $mnav.removeClass 'mnav-closed'
    #     $mnav.addClass 'mnav-open'
    #     $mnav.animate({height: height, width: width})
    #   else
    #     $mnav.removeClass 'mnav-open'
    #     $mnav.addClass 'mnav-closed'
    #     if $(window).outerWidth() < 639
    #       $mnav.animate({height: 65, width: 65})
    #     else
    #       $mnav.animate({height: 65, width: 109})
    #
    #   return

    $c(document).find('.accordion').each () ->
      $(this).addClass 'accordion-closed'
      $(this).find('.accordion-content').animate({height: 0})
      return

    $c(document).on 'click', '.accordion-toggle', ()->
      $accordion = $(this).parent()
      $content = $accordion.find('.accordion-content')
      height = $accordion.find('.accordion-body').outerHeight()

      if $accordion.hasClass 'accordion-closed'
        $accordion.removeClass 'accordion-closed'
        $content.animate({height: height})
      else
        $accordion.addClass 'accordion-closed'
        $content.animate({height: 0})

      return

    $(document).find('.sec-tabs').each () ->
      $positiveBody = $(document).find('.positive-slides')
      $titleList = $(document).find('.title-items')

      $positiveBody.find('.body').each () ->
        $title = $(this).find('.title').text()
        $titleListItem = '<li class="title-item">' + $title + '</li>'
        $titleList.append($titleListItem)
        return

      $titleList.slick({
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
        focusOnSelect: true,
        vertical: true
      })

      $positiveBody.slick({
        asNavFor: $titleList,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
        focusOnSelect: true
      })

    $(document).find('.sec-tabs').each () ->
      $tabsList = $(this).find('.tabs-list')
      $tabsBody = $(this).find('.tabs-display')

      $tabsBody.find('.tab-item').each () ->
        $logo = $(this).find('.logo').attr('src')
        $tabsListItem = '<li class="tabs-list-item"><img class="logo" src="' + $logo + '"></li>'
        $tabsList.append($tabsListItem)
        $button = $(this).find('button').attr('data-bottom','opacity:0;transform:scale(0.95)').attr('data-center-top','opacity:1;transform:scale(1)')
        return

      $tabsBody.slick({
        arrows: false,
        fade: true,
        focusOnSelect: true
      })

      $tabsList.slick({
        asNavFor: $tabsBody,
        focusOnSelect: true,
        slidesToShow: 4,
        arrows: false
      })


    $(document).find('.sec-gallery').each () ->
      $galleryBody = $(this).find('.gallery-display')
      $galleryList = $(this).find('.gallery-list')

      $galleryBody.find('.image').each () ->
        $src = $(this).find('img').attr('src').replace('.jpg', '_thumb.jpg');
        $galleryListItem = '<li class="image"><div class="image-wrap" style="background-image:url(' + $src + ');background-repeat:no-repeat"><img src="' + $src + '"></div></li>'
        $galleryList.append($galleryListItem)
        return

      $galleryBody.slick({
        arrows: true
        fade: true
        focusOnSelect: true
        prevArrow: '<button type="button" class="slick-prev"><</button>'
        nextArrow: '<button type="button" class="slick-next">></button>'
      })

      $galleryList.slick({
        arrows: false
        asNavFor: $galleryBody
        focusOnSelect: true
        slidesToShow: 7
      })


    $(document).find('.sec-leaders').each () ->
      $leadersList = $(this).find('.leaders-list')
      $leadersBody = $(this).find('.leaders-display')

      $leadersBody.find('.leader-item').each () ->
        $profile = $(this).find('.profile').attr('src')
        $name = $(this).find('h3').text()
        $leadersListItem = '<li class="leaders-list-item"><div class="leaders-list-body"><img class="profile" src="' + $profile + '"><div class="name">' + $name + '</div></div></li>'
        $leadersList.append($leadersListItem)
        return

      $leadersBody.slick({
        arrows: false
        fade: true
        focusOnSelect: true
      })

      $leadersList.slick({
        asNavFor: $leadersBody,
        focusOnSelect: true,
        slidesToShow: 4,
        responsive: [
          {
            breakpoint: 640,
            settings: {
              arrows: false,
              slidesToShow: 2
            }
          }
        ]
      })

      $('.leaders-list-item.slick-current').find($('.leaders-list-body')).attr('data-bottom-top', 'opacity:0;transform:rotateY(15deg)').attr('data-center-top', 'opacity:1;transform:rotateY(0deg)')

    getP = () ->
      s = skrollr.init(
        smoothScrolling: false
        forceHeight: false
        mobileCheck: ->
          false
      )

    $c(document).on 'ready', (e) ->
      getP()

    $c(window).on 'load resize', (e) ->
      getP()


    $ ->
      $('a[href*=#]:not([href=#])').click ->
        if location.pathname.replace(/^\//, '') == @pathname.replace(/^\//, '') and location.hostname == @hostname
          target = $(@hash)
          target = if target.length then target else $('[name=' + @hash.slice(1) + ']')
          if target.length
            $('html,body').animate { scrollTop: target.offset().top }, 600
            return false
        return
      return

    cb = ->
      l = document.createElement('link')
      l.rel = 'stylesheet'
      l.href = '//fonts.googleapis.com/css?family=Lato:300,400,700,400italic|Merriweather:300,400,700'
      h = document.getElementsByTagName('head')[0]
      h.parentNode.insertBefore l, h
      return

    raf = requestAnimationFrame or mozRequestAnimationFrame or webkitRequestAnimationFrame or msRequestAnimationFrame
    if raf
      raf cb
    else
      window.addEventListener 'load', cb

  return {
    init: init
  }

)()
